import styles from './headerNav.module.scss'
import Image from 'next/image'
import Link from 'next/link'
import { IoIosArrowDown } from 'react-icons/io'

export default function HeaderNav() {
    return(
        <div className={styles.header}>
            <div className={styles.logo}>
                <Image 
                 src="/images/vector.png"
                 alt="Picture of the author"
                 width={142}
                 height={36}
                />
                <h1 className={styles.logoText}>VAN TILL</h1>
            </div>
            <div className={styles.navbar}>
              <ul className={styles.ulNavbar}>
                <li className={styles.expertiseLi}>
                  <Link href="#home" className={styles.links}>Expertise</Link>
                   <IoIosArrowDown className={styles.arrowIcon}/>
                  </li>
                <li><Link href="#news" className={styles.links}>Team</Link></li>
                <li><Link href="#contact" className={styles.links}>Track Record</Link></li>
                <li><Link href="#about" className={styles.links}>Over Van Till</Link></li>
                <li><Link href="#about" className={styles.links}>Werken bij</Link></li>
                <li><Link href="#about" className={styles.links}>Contact</Link></li>
              </ul>
              <div className={styles.langContent}>
                <p className={styles.langText}>NL</p> 
                <p className={styles.langText}>|</p>
                <p className={styles.langText2}> EN</p>
              </div>
            </div>
        </div>
    )
}