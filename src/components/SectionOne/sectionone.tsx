import styles from './sectionone.module.scss'
import Image from 'next/image'
import { IoIosArrowDown } from 'react-icons/io'

export default function SectionOne() {
    return(
        <div className={styles.sectionContent}>
            <div className={styles.titleContent}>
                <h1 className={styles.title}>Display L advocaten met een sterke reputatie.</h1>
                
                <Image
                    src="/images/section-logo.png"
                    alt="Picture of the author"
                    width={69}
                    height={60}
                />
            </div>
         <div className={styles.imageContent}>
                <Image 
                 src="/images/section-one.png"
                 alt="Picture of the author"
                 width={900}
                 height={645}
                />   
                <div className={styles.iconDiv}>
                    <IoIosArrowDown className={styles.icon}/>
                </div>
         </div>
        </div>
    )
}   